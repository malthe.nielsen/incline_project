from matplotlib import pyplot as plt
import numpy as np
import glob
from scipy.signal import find_peaks
from iminuit import Minuit  
import sys
from scipy import stats
sys.path.append('/Users/nordentoft/Desktop/AppStat2020/External_Functions')
from ExternalFunctions import Chi2Regression



## Importing data measured about the experiment
gate_data = np.genfromtxt('new_measure.csv', delimiter = ',').reshape(4,4)
mean_gate = np.mean(gate_data, axis = 0)*0.01

#Imports Ball diameter D, smallest to biggest
ball_data = np.genfromtxt('ball_size.csv', delimiter = ',').reshape(4,3)
ball_weights = np.asarray([[0.1, 0.1, 0.1],[0.1,0.1,0.1],[0.3,0.3,0.3],[0.1,0.1,0.1]]).reshape(4,3)
ball_size_wa = np.average(ball_data, weights = ball_weights, axis = 0)*0.001


## Defining functions to use later on 
def fit_function(x, alpha0, alpha1):
    return alpha0 + alpha1*x

def grav_func(acc, alpha, D, d):
    g = acc / np.sin(alpha) * (1 + 2/5 * D**2 /(D**2 -d**2))
    return g

def moving_average(x, w): #Useless function, computes running mean
    return np.convolve(x, np.ones(w), 'valid') / w

  
## Alpha 7 calc values
H = 27.1-3.1
L = 90.9
H_err = np.sqrt(2)*0.1
L_err = 0.1

alpha_7_calc = np.arctan(H/L)
alpha_7_calc_err = np.sqrt(np.square(L/(H**2+L**2))*H_err + np.square(-H/(H**2+L**2))*L_err) 

## Alpha 5 calc values
H = 19.2-3.3
L = 90.9
H_err = np.sqrt(2)*0.1
L_err = 0.1

alpha_5_calc = np.arctan(H/L)
alpha_5_calc_err = np.sqrt(np.square(L/(H**2+L**2))*H_err + np.square(-H/(H**2+L**2))*L_err) 

##
def find_acc(dir_name, freedom):
    fnames = sorted(glob.glob(f'./{dir_name}/*.csv'))
    frame = np.zeros((10,4))
    frame_t = np.zeros((10,4))
    for i in range(len(fnames)):
        data = np.genfromtxt(fnames[i], delimiter =',')[1:, :]
        data[:,0] = np.add(data[:,0],-data[0,0])
        inds, _ = find_peaks(data[:,1], height = 4, distance = 100)
        time_int = np.diff(data[inds,0])
        inds = np.where(data[:,1] > 2)
        cum_time = np.cumsum(time_int)
        v = mean_gate / time_int
        frame_t[i,:] = cum_time
        frame[i,:] = v

#  t = np.ndarray.flatten(frame_t)
#  v = np.ndarray.flatten(frame)
    t = np.mean(frame_t, axis = 0)
    v = np.mean(frame, axis = 0)
    vy = mean_gate*0.1
    #  plt.errorbar(t,v, vy)
    #  plt.show()


    chi2_object = Chi2Regression(fit_function, t, v, vy)
    minuit = Minuit(chi2_object, pedantic=False, alpha0=v[0], alpha1=0, fix_alpha0 = False)
    minuit.migrad();  # perform the actual fit
    minuit_output = [minuit.get_fmin(), minuit.get_param_states()] 

    alpha0_fit = minuit.values['alpha0']
    alpha1_fit = minuit.values['alpha1']
    sigma_alpha0_fit = minuit.errors['alpha0']
    sigma_alpha1_fit = minuit.errors['alpha1']

    Chi2_fit = minuit.fval # the chi2 value
    Prob_fit = stats.chi2.sf(Chi2_fit, freedom) 
    return alpha0_fit, sigma_alpha0_fit, alpha1_fit, sigma_alpha1_fit, Prob_fit

dir_list_exp = ['s', 'm', 'b', 'sr', 'mr', 'br', 's7', 'm7', 'b7']
a1_bin = []
a1_err_bin = []
i = 0
for dir_name in dir_list_exp:
    a0, a0_err, a1, a1_err, pf = find_acc(dir_name,2)
    print(grav_func(a1, alpha_5_calc, ball_size_wa[i], 0.08))
    if pf < 0.05:
        print(f'Chivalue too low for {dir_name}, pf value {pf}')
    a1_bin.append(a1)
    a1_err_bin.append(a1_err)
    i += 1

